import React from "react";
import profileDAW from '../images/profilDAW.png'
import {List, ListItemContent} from 'react-mdl'
import {Container, Jumbotron} from "react-bootstrap";
const divStyle = {
    display: 'flex',
    alignItems: 'center'
};
const skill = {
    fontSize: "50px",
    margin: "0 30px 5px 0"
};
class Profile extends React.Component{
    render() {
        let content = {

            EN: {
                title: "Skills",
                },

            RO: {
                title: "Aptitudini",
                }

        };
        content = content.RO;
        return(
            <div>
                <Jumbotron fluid style={divStyle}>
                    <Container>
                        <img
                            className="avatar-img"
                            src={profileDAW}
                            alt="avatar"
                        />
                        <h2 align="center">Cristina Popescu</h2>
                    </Container>
                    <br/>
                    <Container>
                        <h3>{content.title}</h3>
                        <hr/>

                        <div className="contact-list">
                            <link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css"/>
                            <link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css"/>
                            <link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css"/>
                            <link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css"/>
                            <List>
                                    <ListItemContent style={skill}>
                                        <i className="devicon-java-plain-wordmark colored"/>
                                    </ListItemContent>

                                    <ListItemContent style={skill}>
                                        <i className="devicon-csharp-line colored"/>
                                    </ListItemContent>

                                    <ListItemContent style={skill}>
                                        <i className="devicon-swift-plain-wordmark colored"/>
                                    </ListItemContent>

                                    <ListItemContent style={skill}>
                                        <i className="devicon-mysql-plain-wordmark colored"/>
                                    </ListItemContent>
                            </List>
                        </div>
                    </Container>
                </Jumbotron>
            </div>
        );
    }
}

export default Profile;